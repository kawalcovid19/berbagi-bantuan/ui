export const state = () => ({
  location: null, // {lat,lng}
});

export const getters = {};

export const mutations = {
  SET_LOCATION(state, { lat, lng }) {
    state.location = {
      lat,
      lng,
    };
  },
};

export const actions = {
  restoreInitialState({ commit }) {
    commit('SET_LOCATION', null);
  },
  async fetchCurrentPosition({ state, commit }) {
    if (state.location === null) {
      try {
        const pos = await new Promise(function(resolve, reject) {
          navigator.geolocation.getCurrentPosition(resolve, reject);
        });
        commit('SET_LOCATION', {
          lat: pos.coords.latitude,
          lng: pos.coords.longitude,
        });
      } catch (e) {
        console.error(e);
      }
    }
  },
};
