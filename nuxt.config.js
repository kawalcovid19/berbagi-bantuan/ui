/**
 * base url bisa didapatkan dengan login di https://needsmatchmakingbackend.docs.apiary.io/
 * click salah satu api yg tersedia disitu
 * copy paste base urlnya
 */
require('dotenv').config();
module.exports = {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: 'Kawal Kebutuhan',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'Kawal Kebutuhan by KawalCovid19',
        name: 'Kawal Kebutuhan by KawalCovid19',
        content: 'Cari dan salurkan bantuan ke pengguna terverifikasi. Semua dalam satu platform.',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/kawal.ico' }],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#d8232a' },
  /*
   ** Router Setting
   */
  router: {
    linkExactActiveClass: 'active',
  },

  /*
   ** Global CSS
   */
  css: [],
  styleResources: {
    // your settings here
    scss: ['~/assets/scss/variables/_color.scss'],
  },
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '~/plugins/button.js' },
    { src: '~/plugins/vuesweetalert2.js', ssr: false },
    { src: '~/plugins/google-maps.js', ssr: false },
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss',
    // Doc: https://github.com/nuxt-community/color-mode-module
    '@nuxtjs/color-mode',
    //Doc: https://github.com/nuxt-community/style-resources-module#readme
    '@nuxtjs/style-resources',
  ],
  tailwindcss: {
    purgeCSSInDev: false,
    configPath: '~/config/tailwind.config.js',
    cssPath: '~/assets/css/tailwind.css',
    exposeConfig: false,
  },
  //Color mode nuxt
  colorMode: {
    preference: 'dark', // default value of $colorMode.preference
    fallback: 'light', // fallback value if not system preference found
    hid: 'nuxt-color-mode-script',
    globalName: '__NUXT_COLOR_MODE__',
    componentName: 'ColorScheme',
    cookie: {
      key: 'nuxt-color-mode',
      options: {
        // path: nuxt.options.router.base, // https://nuxtjs.org/api/configuration-router#base
      },
    },
  },
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    'nuxt-webfontloader',
    '@nuxtjs/svg',
    [
      'nuxt-mq',
      {
        // Default breakpoint for SSR
        defaultBreakpoint: 'default',
        breakpoints: {
          sm: 640,
          md: Infinity,
        },
      },
    ],
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: process.env.BASE_URL,
    proxy: false,
  },
  /*
   ** Web font loader and purgecss on prod
   **
   */

  webfontloader: {
    google: {
      families: ['IBM+Plex+Sans:400,600', 'IBM+Plex+Mono:400&display=swap'],
    },
  },
  rules: {},
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            fix: true,
          },
        });
      }
    },
    transpile: [/^gmap-vue($|\/)/],
    babel: {
      plugins: ['@babel/plugin-proposal-optional-chaining'],
    },
    loaders: {
      vue: {
        compilerOptions: {
          preserveWhitespace: false,
        },
      },
    },
  },
};
